import {
  Card,
  Container,
  Typography,
  TextField,
  Button,
  Box,
} from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import Question from "../Question/Question";
import Level from "../Level/Level";
import Category from "../Category/Category";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { getAllLevels, getAllCategories, createQuiz } from "../../api";
import { useDispatch, useSelector } from "react-redux";
import {
  setLevels,
  setCategories,
  setSelectedLevel,
  setSelectedCategory,
  setSnackbar,
  closeSnackBar,
} from "../../store/slice/adminSlice";
import CompiledQuestions from "../Question/CompiledQuestions";
import CloseIcon from "@mui/icons-material/Close";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";

const Admin = () => {
  const SEVERITY_ERROR = "error";
  const SEVERITY_SUCCESS = "success";

  const reduxDispatch = useDispatch();
  const [isCreateQuestionLayoutOpen, setIsCreateQuestionLayoutOpen] =
    useState(false);
  const [severityType, setSeverityType] = useState("success");
  const [severityMessage, setSeverityMessage] = useState("");

  const quizName = useRef(null);

  useEffect(() => {
    const fetchAllLevels = async () => {
      let response = await getAllLevels();

      if (response.error) {
        console.log("error", response.error);
      } else {
        reduxDispatch(setLevels({ levels: response.data }));
      }
    };

    const fetchAllCategories = async () => {
      let response = await getAllCategories();

      if (response.error) {
        console.log("error", response.error);
      } else {
        reduxDispatch(setCategories({ categories: response.data }));
      }
    };

    fetchAllLevels();

    fetchAllCategories();
  }, []);

  const handleQuestionLayout = () => {
    isCreateQuestionLayoutOpen
      ? setIsCreateQuestionLayoutOpen(false)
      : setIsCreateQuestionLayoutOpen(true);
  };

  const handleCategories = (event) => {
    reduxDispatch(
      setSelectedCategory({ category: parseInt(event.target.value) })
    );
  };

  const handleLevels = (event) => {
    reduxDispatch(setSelectedLevel({ level: parseInt(event.target.value) }));
  };

  let quiz = useSelector((state) => {
    return state.admin;
  });

  const validateForm = () => {
    let name = quizName.current.value;
    let level = quiz.selectedLevel;
    let category = quiz.selectedCategory;
    let questions = quiz.questions;
    console.log("level", level);

    if (!name) {
      return { error: "Quiz name required!" };
    } else if (!level) {
      return { error: "Level selection required!" };
    } else if (!category) {
      return { error: "category selection required!" };
    } else if (questions.length === 0) {
      return { error: "Atleast one question for quiz required!" };
    }

    return { message: "Form valid" };
  };

  const submitQuestions = async () => {
    let validFormResponse = validateForm();

    if (validFormResponse.error) {
      reduxDispatch(setSnackbar({ snackbarStatus: true }));
      setSeverityMessage(validFormResponse.error);
      setSeverityType(SEVERITY_ERROR);
      return;
    }
    let questionObjectForServer = {
      name: quizName.current.value,
      level: quiz.selectedLevel,
      category: quiz.selectedCategory,
      questions: quiz.questions,
    };

    console.log("finalQuizObject", questionObjectForServer);

    let response = await createQuiz(questionObjectForServer);
    if (response.error) {
      reduxDispatch(setSnackbar({ snackbarStatus: true }));
      setSeverityMessage(response.error);
      setSeverityType(SEVERITY_ERROR);
    } else {
      reduxDispatch(setSnackbar({ snackbarStatus: true }));
      setSeverityMessage(response.data.message);
      setSeverityType(SEVERITY_SUCCESS);
      resetAllInputValues()
    }
  };

  const resetAllInputValues = () =>{
    
  }

  const hideSnackbar = () => {
    reduxDispatch(closeSnackBar({ snackbarStatus: false }));
  };
  return (
    <>
      <Box sx={{ width: "100%", display: "flex", backgroundColor: "#fcf0f0" }}>
        <Box
          sx={{
            width: "20%",
            height: "100vh",
            background:
              "linear-gradient(to right, rgb(245, 128, 148), rgb(252, 74, 104))",
          }}
        >
          <Box sx={{ my: 10 }}>
            <Container sx={{ cursor: "pointer" }}>
              <Typography
                variant="h4"
                sx={{ fontFamily: "playfair-display", color: "white" }}
              >
                Create Quiz
              </Typography>
            </Container>

            <Container sx={{ my: 5, cursor: "pointer" }}>
              <Typography
                variant="h4"
                sx={{ fontFamily: "playfair-display", color: "white" }}
              >
                Create Level
              </Typography>
            </Container>

            <Container sx={{ my: 5, cursor: "pointer" }}>
              <Typography
                variant="h4"
                sx={{ fontFamily: "playfair-display", color: "white" }}
              >
                Create Categories
              </Typography>
            </Container>
          </Box>
        </Box>

        <Container
          sx={{
            width: "80%",
            height: "fit-content",
            display: "flex",
            justifyContent: "center",
            marginTop: 10,
          }}
        >
          <Card sx={{ width: "100%" }}>
            <form>
              <Container sx={{ my: 2 }}>
                <Typography sx={{ my: 1 }}>Quiz Name </Typography>
                <TextField
                  placeholder="Quiz Name"
                  variant="outlined"
                  fullWidth
                  inputRef={quizName}
                />
              </Container>

              <Container sx={{ my: 2 }}>
                <Typography>Select Level</Typography>
                <FormControl sx={{ m: 1, minWidth: 200 }}>
                  <InputLabel>Level</InputLabel>
                  <Select
                    native
                    defaultValue=""
                    label="Grouping"
                    onChange={handleLevels}
                  >
                    <option aria-label="None" value="None" />
                    {quiz.levels.map((level) => {
                      return <Level levelData={level} />;
                    })}
                  </Select>
                </FormControl>
              </Container>

              <Container sx={{ my: 2 }}>
                <Typography>Select Category</Typography>

                <FormControl sx={{ m: 1, minWidth: 200 }}>
                  <InputLabel>Category</InputLabel>
                  <Select
                    native
                    defaultValue=""
                    label="Grouping"
                    onChange={handleCategories}
                  >
                    <option aria-label="None" value="" />
                    {quiz.categories.map((category) => {
                      return <Category categoryData={category} />;
                    })}
                  </Select>
                </FormControl>
              </Container>

              {quiz.questions.map((question) => {
                return <CompiledQuestions quizData={question} />;
              })}
              <Container
                sx={{ display: "flex", justifyContent: "space-between" }}
              >
                {!isCreateQuestionLayoutOpen && (
                  <Button
                    sx={{ my: 2 }}
                    variant="contained"
                    onClick={handleQuestionLayout}
                  >
                    Add New Question
                  </Button>
                )}
                {isCreateQuestionLayoutOpen && (
                  <Button onClick={handleQuestionLayout} variant="contained">
                    <CloseIcon />
                    Close
                  </Button>
                )}
              </Container>
              {isCreateQuestionLayoutOpen && <Question />}

              <Container sx={{ display: "flex", justifyContent: "end", my: 2 }}>
                <Button
                  disabled={false}
                  onClick={submitQuestions}
                  variant="contained"
                >
                  Submit
                </Button>
              </Container>
            </form>

            <Container sx={{ my: 1 }}>
              {quiz.isSnackBarOpen && (
                <Snackbar
                  open={quiz.isSnackBarOpen}
                  autoHideDuration={3000}
                  onClose={hideSnackbar}
                >
                  <Alert severity={severityType}>{severityMessage}</Alert>
                </Snackbar>
              )}
            </Container>
          </Card>
        </Container>
      </Box>
    </>
  );
};

export default Admin;
