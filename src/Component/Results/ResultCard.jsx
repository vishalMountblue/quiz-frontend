import React from "react";
import { Card, Typography } from "@mui/material";

const ResultCard = (props) => {
  let result = props.singleResult;
  

  return (
    <>
      <Card sx={{ p: 5, display: "flex", my: 2 }}>
        <Typography sx={{ width: "10%" }} variant="caption text">
          {props.index}
        </Typography>
        <Typography sx={{ width: "50%" }}>
          {result.attemptQuestion.question}
        </Typography>
        <Typography sx={{ width: "20%", color: result.correct ? "#009933" : "red",fontWeight:700 }}>
          {result.attemptQuestion.selectedOption}
        </Typography>
        <Typography
          sx={{ width: "20%",fontWeight:700 }}
        >
          {result.correctOption}
        </Typography>
      </Card>
    </>
  );
};

export default ResultCard;
