import React, { useEffect } from "react";
import { Header } from "../Header/Header";
import { Box, Card, Container, Typography } from "@mui/material";
import { getQuizResult } from "../../api";
import ResultCard from "./ResultCard";
import { setResult } from "../../store/slice/resultSlice";
import { useDispatch, useSelector } from "react-redux";

const Result = () => {
  const questionSlice = useSelector((state) => state.question);
  const reduxDispatch = useDispatch();
  let score = 0;
  useEffect(() => {
    const getResults = async () => {
      if (questionSlice.attemptedQuestions.length > 0) {
        let response = await getQuizResult(questionSlice.attemptedQuestions);
        if (response.error) {
          console.log("error", response.error);
        } else {
          reduxDispatch(setResult({ result: response.data }));
        }
        console.log("response", response);
      }
    };
    getResults();
  }, []);

  const resultData = useSelector((state) => state.result);

  resultData.result.map((result) => {
    if (result.correct) score++;
  });
  return (
    <>
      <Header />

      <Box sx={{ m: 2 }}>
        <Box sx={{ display: "flex", justifyContent: "space-between" }}>
          <Typography variant="h4">Result</Typography>
          <Typography variant="h4">{`${score} / ${resultData.result.length}`}</Typography>
        </Box>
        <Card sx={{ p: 5, display: "flex", my: 2, fontWeight: 700 }}>
          <Typography sx={{ width: "10%" }} variant="caption text">
            Serial number
          </Typography>
          <Typography sx={{ width: "50%", fontWeight: 700 }}>
            Question
          </Typography>
          <Typography sx={{ width: "20%", fontWeight: 700 }}>
            Selected Option
          </Typography>
          <Typography sx={{ width: "20%", fontWeight: 700 }}>
            Correct option
          </Typography>
        </Card>

        {resultData.result.map((result, index) => {
          if (result.correct) score++;
          return <ResultCard singleResult={result} index={index + 1} />;
        })}
      </Box>
    </>
  );
};

export default Result;
