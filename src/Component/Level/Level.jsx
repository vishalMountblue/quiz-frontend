import React from "react";

const Level = (props) => {
  let levelId = props.levelData.id;
  let level = props.levelData.type;

  return (
    <>
      <option value={levelId}>{level}</option>
    </>
  );
};

export default Level;
