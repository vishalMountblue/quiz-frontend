import { Box, Button, Container, IconButton, Typography } from "@mui/material";
import "./Header.css";
import { Link } from "react-router-dom";
import "./Header.css";
import { Home } from "@mui/icons-material";
import LogoutIcon from "@mui/icons-material/Logout";
import { useDispatch } from "react-redux";
import { resetAttemptedQuestions } from "../../store/slice/questionsSlice";

export const Header = () => {
  const USER = "user";
  const reduxDispatch = useDispatch();

  let loggedInUser = JSON.parse(localStorage.getItem("loggedInUser"));
  let isUser = loggedInUser.user.privilege === USER ? true : false;

  const logout = () => {
    localStorage.clear();
  };

  const clearQuizResponses = () => {
    reduxDispatch(
      resetAttemptedQuestions()
    );
  };

  return (
    <>
      <header>
        <Box>
          <Link to={"/"}>
            <Button
              variant="outlined"
              startIcon={<LogoutIcon />}
              sx={{
                background: "white",
                color: "rgb(252, 74, 104)",
                border: "none",
              }}
              onClick={logout}
            >
              Logout
            </Button>
          </Link>

          <Link to={"/home"}>
            <Button
              variant="outlined"
              onClick={clearQuizResponses}
              startIcon={<Home />}
              sx={{
                background: "white",
                color: "rgb(252, 74, 104)",
                mx: 2,
                border: "none",
              }}
            >
              Home
            </Button>
          </Link>
        </Box>

        <Typography
          sx={{
            fontWeight: 600,
            fontFamily: "Playfair Display",
            my: 0,
            color: "white",
          }}
          variant="h4"
        >
          Welcome to quiz app
        </Typography>

        {!isUser && (
          <Link to={"/admin"}>
            <div>
              <Button variant="contained">Admin</Button>
            </div>
          </Link>
        )}
      </header>
    </>
  );
};
