import React from "react";
import { Header } from "../Header/Header";
import { Box, Card, Container, Typography } from "@mui/material";
import CategoryCard from "../Category/CategoryCard";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { allCategories } from "../../store/slice/homeSlice";

const Home = () => {
  const reduxDispatch = useDispatch();
  let images = ["sports.png", "politics.jpg", "react.jpg"];

  const style = {
    height: "100vh",
    backgroundColor: "#fcf0f0",
  };

  useEffect(() => {
    reduxDispatch(allCategories());
  }, []);

  let homeData = useSelector((state) => {
    return state.home;
  });

  return (
    <>
      <Box sx={style}>
        <Header />

        <Box
          sx={{
            mx: 5,
            height: "90vh",
            display: "flex",

            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Card
            sx={{
              width: "60%",
              height: "90%",
              boxShadow: "0 1px 3px rgba(0, 0, 0, 0.3)",
              background:
                "linear-gradient(to top, rgb(245, 128, 148), rgb(252, 74, 104))",
              display: "flex",

              justifyContent: "center",
              flexDirection: "column",
            }}
          >
            <Container>
              <Typography
                sx={{
                  fontFamily: "Playfair Display",
                  my: 2,
                  color: "white",
                  fontWeight: 600,
                }}
                variant="h4"
              >
                Select Category
              </Typography>
            </Container>

            <Box sx={{ width: "100%", overflow: "auto" }}>
              {homeData.categories.map((category, index) => {
                return (
                  <CategoryCard
                    key={category.id}
                    categoryData={category}
                    image={images[index]}
                  />
                );
              })}
            </Box>
          </Card>
        </Box>
      </Box>
    </>
  );
};

export default Home;
