import { Container, Divider } from "@mui/material";
import React from "react";
import { Card, Typography } from "@mui/material";
import { Link } from "react-router-dom";

import "./Anchor.css";

const CategoryCard = (props) => {
  let category = props.categoryData;

  const style = {
    width: "100%",
    minHeight: 100,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#f9acac",
    color: "white",
    m: 1,
    my: 3,
    mx: 3,
    cursor: "pointer",
  };

  return (
    <>
      <Link to={`/category/${category.id}`} className="link">
        <Divider variant="middle" component="li" />
        <Container
          sx={{
            display: "flex",
            alignItems: "center",
            transition: "transform 1000ms",
            "&:hover": {
              transform: "scale(0.9)",
              transition: "transform 500ms",
            },
          }}
        >
          <Card sx={{ width: 150, height: 125, borderRadius: 50 }}>
            <img src={`./${props.image}`} className="category-image"></img>
          </Card>
          <Card sx={style}>
            <Typography variant="h6" sx={{ fontFamily: "Playfair Display" }}>
              {category.type}
            </Typography>
          </Card>
        </Container>
      </Link>
    </>
  );
};

export default CategoryCard;
