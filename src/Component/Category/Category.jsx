import React from "react";

const Category = (props) => {
  let categoryId = props.categoryData.id;
  let category = props.categoryData.type;
  return (
    <>
      <option value={categoryId}>{category}</option>
    </>
  );
};

export default Category;
