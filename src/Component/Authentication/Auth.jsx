import {
  Box,
  Button,
  Card,
  Container,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import PersonIcon from "@mui/icons-material/Person";
import InputAdornment from "@mui/material/InputAdornment";
import { Password } from "@mui/icons-material";
import { useRef } from "react";
import {
  setNewAccountResponse,
  setLoading,
  setSnackbar,
  closeSnackBar,
} from "../../store/slice/authSlice";

import { createUser, authenticateUser } from "../../api";
import { useDispatch, useSelector } from "react-redux";
import CircularProgress from "@mui/material/CircularProgress";
import Alert from "@mui/material/Alert";
import { useNavigate } from "react-router-dom";

import Snackbar from "@mui/material/Snackbar";
import "./Auth.css";
import * as utils from "../../utils";

const Auth = () => {
  const reduxDispatch = useDispatch();
  const navigate = useNavigate();
  const authData = useSelector((state) => state.auth);

  const [severityType, setSeverityType] = useState("success");
  const [severityMessage, setSeverityMessage] = useState("");

  let username = useRef(null);
  let password = useRef(null);

  const [authDisplay, setAuthDisplay] = useState(utils.displayState);

  const toggleLogInOption = () => {
    authDisplay.loginDisplay
      ? setAuthDisplay(utils.displayState)
      : setAuthDisplay(utils.loginView);

    username.current.value = "";
    password.current.value = "";
  };

  const validateLoginForm = () => {
    if (!username.current.value) {
      return { error: "Username required!" };
    } else if (!password.current.value) {
      return { error: "password required!" };
    }

    return { message: "Form valid" };
  };

  const handleAuthOperation = async () => {
    let validFormResponse = validateLoginForm();
    if (validFormResponse.error) {
      reduxDispatch(setSnackbar({ snackbarStatus: true }));
      setSeverityMessage(validFormResponse.error);
      setSeverityType(utils.SEVERITY_ERROR);
      return;
    }

    if (authDisplay.authOperation === utils.CREATE) {
      reduxDispatch(setLoading({ loading: true }));

      let response = await createUser(
        username.current.value,
        password.current.value
      );

      if (response.error) {
        console.log("error", response.error);
      } else {
        let createAccount = response.data;
        reduxDispatch(setNewAccountResponse({ createAccount }));

        reduxDispatch(setSnackbar({ snackbarStatus: true }));
        setSeverityMessage(createAccount.message);

        if (createAccount.userCreated) setSeverityType(utils.SEVERITY_SUCCESS);
        else setSeverityType(utils.SEVERITY_ERROR);
      }
    } else {
      let response = await authenticateUser(
        username.current.value,
        password.current.value
      );

      if (response.error) {
        console.log("error", response.error);
      } else {
        console.log("response", response);

        if (response.data.auth) {
          let loggedInUserObject = JSON.stringify(response.data);
          localStorage.setItem("loggedInUser", loggedInUserObject);
          navigate("/home");
        } else {
          reduxDispatch(setSnackbar({ snackbarStatus: true }));
          setSeverityMessage(response.data.message);
          setSeverityType(utils.SEVERITY_ERROR);
        }
      }
    }

    username.current.value = "";
    password.current.value = "";
  };

  const hideSnackbar = () => {
    reduxDispatch(closeSnackBar({ snackbarStatus: false }));
  };

  let style = {
    height: "100vh",
    display: "flex",
    backgroundColor: "#fcf0f0",
  };
  return (
    <>
      <Box sx={style}>
        <Box
          sx={{
            width: "30%",
            height: "100%",
            background:
              "linear-gradient(to right, rgb(245, 128, 148), rgb(252, 74, 104))",
          }}
        >
          <Typography
            variant="h3"
            sx={{ color: "white", marginTop: 10, mx: 2 }}
          >
            Hello!
          </Typography>
          <Typography variant="h4" sx={{ color: "white", marginTop: 2, mx: 2 }}>
            Ready to take up a quiz ?
          </Typography>
        </Box>
        <Container
          sx={{
            display: "flex",
            justifyContent: "center",
            width: "70%",
            height: "100%",
            alignItems: "center",
          }}
        >
          <Card
            sx={{
              width: "60%",
              height: "50%",
              p: 3,
              boxShadow: "0 1px 3px rgba(0, 0, 0, 0.3)",
              display: "flex",
              flexDirection: "column",
            }}
          >
            {authData.loading && (
              <CircularProgress
                sx={{
                  position: "relative",
                  left: "50%",
                  top: "50%",
                  zIndex: 1,
                }}
              />
            )}
            <Typography variant="h4" sx={{}}>
              {authDisplay.heading}
            </Typography>

            <TextField
              placeholder="Username"
              defaultValue={authDisplay.defaultTextFieldValue}
              inputRef={username}
              sx={{ my: 4 }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <PersonIcon />
                  </InputAdornment>
                ),
              }}
            ></TextField>
            <TextField
              placeholder="Password"
              defaultValue={authDisplay.defaultTextFieldValue}
              inputRef={password}
              sx={{ my: 2 }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Password />
                  </InputAdornment>
                ),
              }}
            ></TextField>

            <Button
              variant="contained"
              sx={{ height: "10%", marginTop: 2 }}
              onClick={handleAuthOperation}
            >
              {authDisplay.buttonText}
            </Button>

            <Typography
              onClick={toggleLogInOption}
              sx={{ marginTop: 3, cursor: "pointer", color: "blue" }}
            >
              {authDisplay.toggleLogInOptionText}
            </Typography>

            <Container sx={{ my: 1 }}>
              {authData.isSnackBarOpen && (
                <Snackbar
                  open={authData.isSnackBarOpen}
                  autoHideDuration={3000}
                  onClose={hideSnackbar}
                >
                  <Alert severity={severityType}>{severityMessage}</Alert>
                </Snackbar>
              )}
            </Container>
          </Card>
        </Container>
      </Box>
    </>
  );
};

export default Auth;
