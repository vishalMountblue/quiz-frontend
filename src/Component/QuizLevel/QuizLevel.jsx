import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { allLevels } from "../../store/slice/homeSlice";
import LevelCard from "./LevelCard";
import { Header } from "../Header/Header";
import { Box, Card, Container, Typography } from "@mui/material";
import "./Anchor.css";
import { useParams } from "react-router-dom";

const QuizLevel = () => {
  let images = ["easy.gif", "medium.png", "hard.jpg"];
  let categoryId = useParams();

  const reduxDispatch = useDispatch();

  useEffect(() => {
    reduxDispatch(allLevels());
  }, []);

  let homeData = useSelector((state) => {
    return state.home;
  });
  return (
    <>
      <Box sx={{ height: "100vh", backgroundColor: "#fcf0f0" }}>
        <Header />

        <Box
          sx={{
            mx: 5,
            height: "90vh",
            display: "flex",

            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Card
            sx={{
              width: "60%",
              height: "90%",
              boxShadow: "0 1px 3px rgba(0, 0, 0, 0.3)",
              background:
                "linear-gradient(to top, rgb(245, 128, 148), rgb(252, 74, 104))",
              display: "flex",

              justifyContent: "center",
              flexDirection: "column",
            }}
          >
            <Container>
              <Typography
                sx={{
                  fontFamily: "Playfair Display",
                  my: 2,
                  color: "white",
                  fontWeight: 600,
                }}
                variant="h4"
              >
                Select Level
              </Typography>
            </Container>

            <Box sx={{ width: "100%", overflow: "auto" }}>
              {homeData.levels.map((level, index) => {
                return (
                  <LevelCard
                    key={level.id}
                    levelData={level}
                    image={images[index]}
                    categoryId={categoryId}
                  />
                );
              })}
            </Box>
          </Card>
        </Box>
      </Box>
    </>
  );
};

export default QuizLevel;
