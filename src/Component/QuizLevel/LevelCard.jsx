import {Container, Divider } from "@mui/material";
import React from "react";
import { Card, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import "./Anchor.css";

const LevelCard = (props) => {
  let level = props.levelData;
  let categoryId = props.categoryId

  return (
    <>
      <Link to={`/category/${categoryId.category}/level/${level.id}`} className="link">
        <Divider variant="middle" component="li" />
        <Container
          sx={{
            display: "flex",
            alignItems: "center",
            transition: "transform 1000ms",
            "&:hover": {
              transform: "scale(0.9)",
              transition: "transform 500ms",
            },
          }}
        >
          <Card
            sx={{
              width: 150,
              height: 125,
              borderRadius: 50,
            }}
          >
            <img src={`/${props.image}`} className="category-image"></img>
          </Card>
          <Card
            sx={{
              width: "100%",
              minHeight: 100,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#f9acac",
              color: "white",
              m: 1,
              my: 3,
              mx: 3,
              cursor: "pointer",
            }}
           
          >
            <Typography variant="h6" sx={{ fontFamily: "Playfair Display" }}>
              {level.type}
            </Typography>
          </Card>
        </Container>
      </Link>
    </>
  );
};

export default LevelCard;
