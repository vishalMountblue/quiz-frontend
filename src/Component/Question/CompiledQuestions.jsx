import React, { useState } from "react";
import {
  Card,
  Typography,
  Container,
  Button,
  IconButton,
  Box,
} from "@mui/material";
import QuestionDetail from "./QuestionDetail";
import DeleteIcon from "@mui/icons-material/Delete";
import { useDispatch } from "react-redux";
import { scrapQuestion } from "../../store/slice/adminSlice";

const CompiledQuestions = (props) => {
  const [isQuestionView, setIsQuestionView] = useState(false);

  const handleQuestionView = () => {
    isQuestionView ? setIsQuestionView(false) : setIsQuestionView(true);
  };

  const reduxDispatch = useDispatch();

  const deleteQuestion = () => {
    let id = props.quizData.id;
    reduxDispatch(scrapQuestion({ questionId: id }));
  };

  let quizData = props.quizData;

  return (
    <>
      {
        <Container sx={{ my: 2 }}>
          <Card
            sx={{
              p: 2,
              background: "pink",
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <Typography>{props.quizData.question} </Typography>
            <Box>
              <Button variant="contained" onClick={handleQuestionView}>
                View
              </Button>
              <IconButton onClick={deleteQuestion}>
                <DeleteIcon sx={{ color: "red" }} />
              </IconButton>
            </Box>

            {isQuestionView && (
              <QuestionDetail
                openState={isQuestionView}
                close={handleQuestionView}
                quiz={quizData}
              />
            )}
          </Card>
        </Container>
      }
    </>
  );
};

export default CompiledQuestions;
