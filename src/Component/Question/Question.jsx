import {
  Button,
  Card,
  TextField,
  Typography,
  Container,
  Box,
} from "@mui/material";
import React, { useState } from "react";
import { useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setQuestions, setCorrectOption } from "../../store/slice/adminSlice";
import Radio from "@mui/material/Radio";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";

const Question = () => {
  const reduxDispatch = useDispatch();

  const question = useRef(null);
  const firstOption = useRef(null);
  const secondOption = useRef(null);
  const thirdOption = useRef(null);
  const fourthOption = useRef(null);

  const handleCorrectOption = (event) => {
    reduxDispatch(setCorrectOption({ selectedOption: parseInt(event.target.value) }));
  };

  let quizData = useSelector((state) => {
    console.log("quiz object", state.admin.questions);
    return state.admin;
  });

  const addQuestion = () => {
    let quizObject = {
      id: crypto.randomUUID(),
      question: question.current.value,
      options: [
        firstOption.current.value,

        secondOption.current.value,

        thirdOption.current.value,

        fourthOption.current.value,
      ],
      correctOption: quizData.correctOption,
    };

    reduxDispatch(setQuestions({ question: quizObject }));
  };

  return (
    <>
      <Box sx={{ my: 2 }}>
        <Container sx={{ mx: 0 }}>
          <Typography sx={{ my: 1 }}>Question </Typography>
          <TextField
            inputRef={question}
            placeholder="Type your question"
            variant="outlined"
            fullWidth
          />
        </Container>

        <Container sx={{ display: "flex" }}>
          <Container sx={{ my: 2 }}>
            <Typography sx={{ my: 1 }}>Option 1 </Typography>
            <TextField
              id="radio"
              inputRef={firstOption}
              variant="outlined"
              fullWidth
            />

            <Typography sx={{ my: 1 }}>Option 3 </Typography>
            <TextField inputRef={thirdOption} variant="outlined" fullWidth />
          </Container>

          <Container sx={{ my: 2 }}>
            <Typography sx={{ my: 1 }}>Option 2 </Typography>
            <TextField inputRef={secondOption} variant="outlined" fullWidth />

            <Typography sx={{ my: 1 }}>Option 4 </Typography>
            <TextField inputRef={fourthOption} variant="outlined" fullWidth />
          </Container>
        </Container>

        <Container sx={{ my: 2 }}>
          <FormControl sx={{ m: 1, minWidth: 200 }}>
            <InputLabel htmlFor="grouped-native-select">
              Correct option
            </InputLabel>
            <Select
              native
              defaultValue=""
              label="Grouping"
              onChange={handleCorrectOption}
            >
              <option aria-label="None" value="" />
              <option value={"1"}>Option 1</option>
              <option value={"2"}>Option 2</option>
              <option value={"3"}>Option 3</option>
              <option value={"4"}>Option 4</option>
            </Select>
          </FormControl>
        </Container>

        <Button onClick={addQuestion} variant="contained">
          Add
        </Button>
      </Box>
    </>
  );
};

export default Question;
