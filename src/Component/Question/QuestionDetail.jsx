import React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import { Container, IconButton } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";

const QuestionDetail = (props) => {
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 900,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  console.log("props in qestion detail", props);
  let question = props.quiz.name;
  let firstOption = props.quiz.options[0];
  let secondOption = props.quiz.options[1];
  let thirdOption = props.quiz.options[2];
  let fourthOption = props.quiz.options[3];
  return (
    <>
      <Modal open={props.openState} onClose={props.close}>
        <Box sx={style}>
          <Box sx={{ display: "flex", justifyContent: "end" }}>
            <IconButton>
              <EditIcon sx={{ color: "green" }} />
            </IconButton>
          </Box>

          <Typography
            variant="caption text"
            sx={{ fontWeight: 600, color: "#FF8886" }}
          >
            Question
          </Typography>

          <Typography>{question} </Typography>
          <Container sx={{ display: "flex" }}>
            <Container>
              <Typography
                variant="caption text"
                sx={{ fontWeight: 600, color: "#FF8886" }}
              >
                Option 1
              </Typography>
              <Typography>{firstOption}</Typography>

              <Typography
                variant="caption text"
                sx={{ fontWeight: 600, color: "#FF8886" }}
              >
                Option 4
              </Typography>
              <Typography>{fourthOption}</Typography>
            </Container>

            <Container>
              <Typography
                variant="caption text"
                sx={{ fontWeight: 600, color: "#FF8886" }}
              >
                Option 2
              </Typography>
              <Typography>{secondOption}</Typography>
              <Typography
                variant="caption text"
                sx={{ fontWeight: 600, color: "#FF8886" }}
              >
                Option 3
              </Typography>
              <Typography>{thirdOption}</Typography>
            </Container>
          </Container>
        </Box>
      </Modal>
    </>
  );
};

export default QuestionDetail;
