import React from "react";
import { Card, Typography, Box, Container, Button } from "@mui/material";
import { Link } from "react-router-dom";
import "./QuizCard.css";

const QuizCard = (props) => {
  let quiz = props.quizData;
  return (
    <>
      <Link to={`/quiz/${quiz.id}`} className="link">
        <Card
          sx={{
            width: "80%",
            minHeight: 350,

            background:
              "linear-gradient(to right, rgb(245, 128, 148), rgb(252, 74, 104))",
            color: "white",
            m: 1,
            cursor: "pointer",
          }}
        >
          <Box sx={{ height: "50%", width: "100%" }}>
            <img src="/sports.png"></img>
          </Box>
          <Box
            sx={{
              height: "50%",
            }}
          >
            <Container
              sx={{ display: "flex", justifyContent: "space-between" }}
            >
              <Typography variant="h6" sx={{ fontFamily: "Playfair Display" }}>
                {quiz.name}
              </Typography>
              <Typography variant="h6" sx={{ fontFamily: "Playfair Display" }}>
                {quiz.number_of_questions} Questions
              </Typography>
            </Container>

            <Container sx={{ display: "flex", justifyContent: "center",my:5 }}>
              <Button sx={{background:"rgb(245, 128, 148)","&:hover": {
              transform: "scale(1.1)",
              transition: "transform 300ms",
              background:"#fecdd5"
            },}} variant="contained">Take Quiz</Button>
            </Container>
          </Box>
        </Card>
      </Link>
    </>
  );
};

export default QuizCard;
