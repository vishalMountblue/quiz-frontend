import React, { useEffect } from "react";
import { Header } from "../Header/Header";
import { Box, Typography } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import { getAllQuizzes } from "../../api";
import { allQuizzes, setQuizzes } from "../../store/slice/homeSlice";
import QuizCard from "./QuizCard";
import { useParams } from "react-router-dom";

const Quizzes = () => {
  let id = useParams();
  const reduxDispatch = useDispatch();

  let selectedLevelId = parseInt(id.level);
  let selectedCategoryId = parseInt(id.category);
  let values = {
    selectedLevelId,
    selectedCategoryId,
  };

  useEffect(() => {
    reduxDispatch(allQuizzes(values));
  }, []);

  const homeData = useSelector((state) => state.home);
  console.log("Quizzes", homeData.quizzes);
  return (
    <>
      <Header />

      <Box sx={{ p: 1, backgroundColor: "#fcf0f0" }}>
        <Typography
          sx={{
            fontFamily: "Playfair Display",
            my: 2,
            mx: 2,
            fontWeight: 600,
            display: "flex",
          }}
          variant="h4"
        >
          Quizzes
        </Typography>

        <Box sx={{ display: "flex", flexWrap: "wrap", width: "100%" }}>
          {homeData.quizzes.map((quiz) => {
            return <QuizCard key={quiz.id} quizData={quiz} />;
          })}
        </Box>
      </Box>
    </>
  );
};

export default Quizzes;
