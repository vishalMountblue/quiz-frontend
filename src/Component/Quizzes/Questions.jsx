import { Container, Typography, Box, Card, Button } from "@mui/material";
import React from "react";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import { useState } from "react";
import "./QuizCard.css";
import { useDispatch } from "react-redux";
import {
  setAttemptedQuestions,
  setCurrentQuestionCount,
} from "../../store/slice/questionsSlice";
import { useNavigate } from "react-router-dom";

const Questions = (props) => {
  const ques = props.questions;
  const options = ques.options;
  const totalQuestions = props.numberOfQuestions;
  const quizId = props.quizId;
  const reduxDispatch = useDispatch();
  const navigate = useNavigate();
  let currentCount = props.count;

  let firstOption = options[0];
  let secondOption = options[1];
  let thirdOption = options[2];
  let fourthOption = options[3];

  const [value, setValue] = useState("");

  let optionObject = {
    [firstOption.id]: false,
    [secondOption.id]: false,
    [thirdOption.id]: false,
    [fourthOption.id]: false,
  };

  const [selectedOption, setSelectedOption] = useState(optionObject);

  const handleChange = (event) => {
    console.log("event.target.value", JSON.parse(event.target.value));
    setSelectedOption({
      ...optionObject,
      [JSON.parse(event.target.value).id]: true,
    });
    setValue(JSON.parse(event.target.value));

    console.log("optionObject", selectedOption);
  };

  const nextQuestion = (e) => {
    if (!value) {
      alert("Please choose a option!");
      return;
    }

    if (currentCount === totalQuestions - 2) e.target.innerText = "Submit";
    console.log("value", value);

    let attemptedQuestionObject = {
      question: ques.question,
      question_id: ques.id,
      selectedOption: value.option,
      option_id: value.id,
      quiz_id: quizId,
    };
    console.log("attemptedQuestionObject", attemptedQuestionObject);
    reduxDispatch(
      setAttemptedQuestions({ attemptedQuestion: attemptedQuestionObject })
    );

    reduxDispatch(setCurrentQuestionCount({ currentCount: ++currentCount }));
    setValue("");
  };

  return (
    <>
      <Container sx={{ width: "100%", height: "100%", my: 2 }}>
        <Typography align="right" sx={{ fontWeight: 600 }}>
          {currentCount + 1} / {totalQuestions}
        </Typography>
        <Box
          sx={{
            width: "100%",
            height: "50%",
            my: 2,
          }}
        >
          <Typography variant="h5">{ques.question}</Typography>
        </Box>
        <Box sx={{ width: "100%", height: "50%", my: 2 }}>
          <FormControl sx={{ width: "100%" }}>
            <RadioGroup onChange={handleChange} value={value}>
              <FormControlLabel
                id="option1"
                value={JSON.stringify(firstOption)}
                control={<Radio sx={{ opacity: 0 }} />}
                sx={{ width: "100%", display: "inline-block" }}
                label={
                  <Card
                    sx={{
                      my: 0,
                      p: 2,
                      mx: 0,
                      background: selectedOption[firstOption.id]
                        ? "pink"
                        : "white",
                    }}
                  >
                    <Typography>{firstOption.option}</Typography>
                  </Card>
                }
              />

              <FormControlLabel
                id="option2"
                value={JSON.stringify(secondOption)}
                control={<Radio sx={{ opacity: 0 }} />}
                sx={{ width: "100%", display: "inline-block" }}
                label={
                  <Card
                    sx={{
                      my: 0,
                      p: 2,
                      mx: 0,
                      background: selectedOption[secondOption.id]
                        ? "pink"
                        : "white",
                    }}
                  >
                    <Typography>{secondOption.option}</Typography>
                  </Card>
                }
              />

              <FormControlLabel
                id="option3"
                value={JSON.stringify(thirdOption)}
                sx={{ width: "100%", display: "inline-block" }}
                control={<Radio sx={{ opacity: 0 }} />}
                label={
                  <Card
                    sx={{
                      my: 0,
                      p: 2,
                      mx: 0,
                      background: selectedOption[thirdOption.id]
                        ? "pink"
                        : "white",
                    }}
                  >
                    <Typography>{thirdOption.option}</Typography>
                  </Card>
                }
              />

              <FormControlLabel
                id="option4"
                value={JSON.stringify(fourthOption)}
                sx={{ width: "100%", display: "inline-block" }}
                control={<Radio sx={{ opacity: 0 }} />}
                label={
                  <Card
                    sx={{
                      my: 0,
                      p: 2,
                      mx: 0,
                      background: selectedOption[fourthOption.id]
                        ? "pink"
                        : "white",
                    }}
                  >
                    <Typography>{fourthOption.option}</Typography>
                  </Card>
                }
              />
            </RadioGroup>
          </FormControl>
        </Box>
        <Container sx={{ display: "flex", justifyContent: "end" }}>
          <Button
            onClick={nextQuestion}
            sx={{ my: 2 }}
            variant="contained"
            size="large"
          >
            Next
          </Button>
        </Container>
      </Container>
    </>
  );
};

export default Questions;
