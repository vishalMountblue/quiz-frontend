import React, { useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { getAllQuestionsWithOptions } from "../../api";
import { Box, Card } from "@mui/material";
import { Header } from "../Header/Header";
import Questions from "./Questions";
import { getAllQuestions, setQuestions } from "../../store/slice/questionsSlice";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

const QuizQuestions = () => {
  const quiz = useParams();
  let quizId = quiz.id
  const reduxDispatch = useDispatch();
  const navigate = useNavigate();


  let questionData = useSelector((state) => state.question);

  useEffect(() => {
    reduxDispatch(getAllQuestions(quizId));
  }, []);

  if (
    questionData.questions.length > 0 &&
    questionData.currentQuestionCount === questionData.questions.length
  ) {
    
    // <Link redirect="/quiz/results" ></Link>
    navigate("/quiz/results");

    return;
  }

  // useEffect(() => {
  //   const navigateToResultPage = async () => {
  //     if (
  //       questionData.questions.length > 0 &&
  //       questionData.currentQuestionCount === questionData.questions.length
  //     ) {
  //       localStorage.setItem(
  //         "quiz",
  //         JSON.stringify(questionData.attemptedQuestions)
  //       );
  //       // <Link redirect="/quiz/results" ></Link>
  //       navigate("/quiz/results");

  //       return;
  //     }
  //   };
  //   navigateToResultPage();
  // }, [questionData.currentQuestionCount]);

  return (
    <>
      <Header />
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <Card
          sx={{
            width: "50%",
            minHeight: 350,

            background:
              "linear-gradient(to right, rgb(245, 128, 148), rgb(252, 74, 104))",
            color: "white",
            m: 1,
            cursor: "pointer",
          }}
        >
          {questionData.questions.length > 0 &&
          questionData.currentQuestionCount < questionData.questions.length ? (
            <Questions
              questions={
                questionData.questions[questionData.currentQuestionCount]
              }
              numberOfQuestions={questionData.questions.length}
              count={questionData.currentQuestionCount}
              quizId={quizId}
            />
          ) : (
            ""
          )}
        </Card>
      </Box>
    </>
  );
};

export default QuizQuestions;
