export const LOGIN = "login";
export const CREATE = "create account";
export const SEVERITY_ERROR = "error";
export const SEVERITY_SUCCESS = "success";

export const loginView = {
  heading: "Login",
  buttonText: "Login",
  toggleLogInOptionText: "New User? Click to Sign up!",
  authOperation: LOGIN,
  loginDisplay: true,
};
export const displayState = {
  heading: "Create Account",
  buttonText: "Create",
  toggleLogInOptionText: "Already have an account? Click to Log in!",
  authOperation: CREATE,
  loginDisplay: false,
  defaultTextFieldValue: "",
};
