import axios from "axios";
export const getAllLevels = async () => {
  try {
    let levelsResponse = await axios.get("http://localhost:5000/levels");
    return levelsResponse;
  } catch (err) {
    return { error: err.message };
  }
};

export const getAllCategories = async () => {
  console.log("making api call");
  try {
    let categoriesResponse = await axios.get(
      "http://localhost:5000/categories"
    );
    return categoriesResponse;
  } catch (err) {
    return { error: err.message };
  }
};

export const createQuiz = async (questionObjectForServer) => {
  try {
    let quizResponse = await axios.post("http://localhost:5000/quiz", {
      quiz: questionObjectForServer,
    });
    return quizResponse;
  } catch (err) {
    return { error: err.message };
  }
};

export const getAllQuizzes = async (categoryId, levelId) => {
  try {
    let allQuizzes = await axios.get("http://localhost:5000/quiz", {
      params: { categoryId, levelId },
    });
    return allQuizzes;
  } catch (err) {
    return { error: err.message };
  }
};

export const getAllQuestionsWithOptions = async (quizId) => {
  console.log("inside quiz api")
  try {
    let allQuizzes = await axios.get("http://localhost:5000/quiz/questions", {
      params: { quizId },
    });
    return allQuizzes;
  } catch (err) {
    return { error: err.message };
  }
};

export const getQuizResult = async (attemptedQuestions) => {
  try {
    let quizResultResponse = await axios.post("http://localhost:5000/quiz/results", {
      quiz: attemptedQuestions,
    });
    return quizResultResponse;
  } catch (err) {
    return { error: err.message };
  }
};

// auth api

export const createUser = async (username, password) => {
  try {
    let createUserResponse = await axios.post(
      "http://localhost:5000/authentication/create-user",
      {
        username,
        password,
        privilege: "user",
      }
    );
    return createUserResponse;
  } catch (err) {
    console.log("error in api", err);
    return { error: err.message };
  }
};

export const authenticateUser = async (username, password) => {
  try {
    let loginResponse = await axios.post(
      "http://localhost:5000/authentication/login",
      {
        username,
        password,
      }
    );
    return loginResponse;
  } catch (err) {
    return { error: err.message };
  }
};
