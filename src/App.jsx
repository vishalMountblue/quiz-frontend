import "./App.css";
import Home from "./Component/Home/Home";
import { Routes, Route } from "react-router-dom";
import Admin from "./Component/Admin/Admin";
import QuizLevel from "./Component/QuizLevel/QuizLevel";
import Quizzes from "./Component/Quizzes/Quizzes";
import Auth from "./Component/Authentication/Auth";
import QuizQuestions from "./Component/Quizzes/QuizQuestions";
import {  useNavigate } from "react-router-dom";
import { useEffect } from "react";
import Result from "./Component/Results/Result";

function App() {
  const navigate = useNavigate();
  useEffect(() => {
    let loggedInUser = JSON.parse(localStorage.getItem("loggedInUser"));
    if (loggedInUser) navigate("/home");
  }, []);
  return (
    <>
      <Routes>
        <Route path="/" element={<Auth />}></Route>
        <Route path="/home" element={<Home />}></Route>
        <Route path="/category/:category" element={<QuizLevel />}></Route>
        <Route path="/admin" element={<Admin />}></Route>
        <Route path="/category/:category/level/:level" element={<Quizzes />}></Route>
        <Route path="/quiz/:id" element={<QuizQuestions />}></Route>
        <Route path="/quiz/results" element={<Result />}></Route>
      </Routes>
    </>
  );
}

export default App;
