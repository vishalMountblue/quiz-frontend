import { createSlice } from "@reduxjs/toolkit";
import React from "react";

const authSlice = createSlice({
  name: "auth",
  initialState: {
    createAccount: {},
    
    loading: false,
    isSnackBarOpen: false,
    snackbarType: null,
  },
  reducers: {
    setLoading(state, action) {
      state.loading = action.payload.snackbarStatus;
    },
    setSnackbar(state, action) {
      state.isSnackBarOpen = action.payload.snackbarStatus;
    },
    setSnackbarType(state, action) {
      state.snackbarType = action.payload.snackbarType;
    },
    setNewAccountResponse(state, action) {
      (state.createAccount = action.payload.createAccount),
        (state.loading = false);
    },
   

    closeSnackBar(state, action) {
      state.isSnackBarOpen = action.payload.snackbarStatus;
    },
  },
});

export default authSlice.reducer;
export const {
  setNewAccountResponse,
  setLoading,
  closeSnackBar,
  setSnackbar,
  setSnackbarType,
 
} = authSlice.actions;
