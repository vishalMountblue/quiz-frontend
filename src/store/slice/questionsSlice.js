import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { getAllQuestionsWithOptions } from "../../api";

export const getAllQuestions = createAsyncThunk(
  "questions",
  async (quizId, { rejectWithValue }) => {
    let response = await getAllQuestionsWithOptions(quizId);
    if (response.error) {
      return rejectWithValue(response.error);
    } else {
      return { questions: response.data };
    }
  }
);

const questionsSlice = createSlice({
  name: "questions",
  initialState: {
    questions: [],
    attemptedQuestions: [],
    currentQuestionCount: 0,
    loading: false,
    error: null,
  },
  reducers: {
    
    setCurrentQuestionCount(state, action) {
      state.currentQuestionCount = action.payload.currentCount;
    },
    setAttemptedQuestions(state, action) {
      state.attemptedQuestions.push(action.payload.attemptedQuestion);
    },
    resetAttemptedQuestions(state, action) {
      state.questions = [];
      state.attemptedQuestions = [];
      state.currentQuestionCount = 0;
    },
  },
  extraReducers: {
    [getAllQuestions.pending]: (state) => {
      state.loading = true;
    },
    [getAllQuestions.fulfilled]: (state, action) => {
      state.loading = false;
      state.questions = action.payload.questions;
    },
    [getAllQuestions.rejected]: (state, action) => {
      (state.loading = false), (state.error = action.payload);
    },
  },
});

export default questionsSlice.reducer;
export const {
  setQuestions,
  setCurrentQuestionCount,
  setAttemptedQuestions,
  resetAttemptedQuestions,
} = questionsSlice.actions;
