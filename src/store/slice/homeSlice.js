import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import React from "react";
import { getAllCategories, getAllLevels, getAllQuizzes } from "../../api";  

export const allCategories = createAsyncThunk(
  "categories",
  async (args, { rejectWithValue }) => {
    let response = await getAllCategories();

    if (response.error) {
      return rejectWithValue(response.error);
    } else {
      return { categories: response.data };
    }
  }
);

export const allLevels = createAsyncThunk(
  "levels",
  async (args, { rejectWithValue }) => {
    let response = await getAllLevels();

    if (response.error) {
      return rejectWithValue(response.error);
    } else {
      return { levels: response.data };
    }
  }
);

export const allQuizzes = createAsyncThunk(
  "quizzes",
  async (args, { rejectWithValue }) => {
    let response = await getAllQuizzes(
      args.selectedCategoryId,
      args.selectedLevelId
    );

    if (response.error) {
      return rejectWithValue(response.error);
    } else {
      return { quizzes: response.data };
    }
  }
);

const homeSlice = createSlice({
  name: "home",
  initialState: {
    categories: [],
    levels: [],
    selectedCategoryId: null,
    selectedLevelId: null,
    quizzes: [],
    loading: false,
    error: null,
  },
  extraReducers: {
    [allCategories.pending]: (state) => {
      state.loading = true;
    },
    [allCategories.fulfilled]: (state, action) => {
      state.loading = false;
      state.categories = action.payload.categories;
    },
    [allCategories.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },

    [allLevels.pending]: (state) => {
      state.loading = false;
    },
    [allLevels.fulfilled]: (state, action) => {
      state.loading = false;
      state.levels = action.payload.levels;
    },
    [allLevels.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },

    [allQuizzes.pending]: (state) => {
      state.loading = false;
    },
    [allQuizzes.fulfilled]: (state, action) => {
      state.loading = false;
      state.quizzes = action.payload.quizzes;
    },
    [allQuizzes.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export default homeSlice.reducer;
export const { setQuizzes } = homeSlice.actions;
