import { createSlice } from "@reduxjs/toolkit";
import React from "react";

const resultSlice = createSlice({
  name: "result",
  initialState: {
    result: [],
  },
  reducers: {
    setResult(state, action) {
      state.result = action.payload.result;
    },
  },
});

export default resultSlice.reducer;
export const {setResult} = resultSlice.actions
