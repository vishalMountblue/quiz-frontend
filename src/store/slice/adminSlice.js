import { createSlice } from "@reduxjs/toolkit";
import React from "react";

const adminSlice = createSlice({
  name: "admin",
  initialState: {
    levels: [],
    categories: [],
    questions: [],
    selectedLevel: "",
    selectedCategory: "",
    correctOption: "",
    isSnackBarOpen: false,
  },
  reducers: {
    setLevels(state, action) {
      state.levels = action.payload.levels;
    },
    setCategories(state, action) {
      state.categories = action.payload.categories;
    },
    setQuestions(state, action) {
      state.questions.push(action.payload.question);
    },
    scrapQuestion(state, action) {
      state.questions = state.questions.filter((question) => {
        if (question.id === action.payload.questionId) return null;
        return question;
      });
    },
    setSelectedLevel(state, action) {
      state.selectedLevel = action.payload.level;
    },
    setSelectedCategory(state, action) {
      state.selectedCategory = action.payload.category;
    },
    setCorrectOption(state, action) {
      state.correctOption = action.payload.selectedOption;
    },
    setSnackbar(state, action) {
      state.isSnackBarOpen = action.payload.snackbarStatus;
    },
    closeSnackBar(state, action) {
      state.isSnackBarOpen = action.payload.snackbarStatus;
    },
  },
});

export default adminSlice.reducer;
export const {
  setLevels,
  setCategories,
  setQuestions,
  scrapQuestion,
  setSelectedLevel,
  setSelectedCategory,
  setCorrectOption,
  setSnackbar,
  closeSnackBar,
} = adminSlice.actions;
