import { configureStore } from "@reduxjs/toolkit";
import adminSlice from "./slice/adminSlice";
import homeSlice from "./slice/homeSlice";
import authSlice from "./slice/authSlice";
import questionsSlice from "./slice/questionsSlice";
import resultSlice from "./slice/resultSlice";

const store = configureStore({
  reducer: {
    admin: adminSlice,
    home: homeSlice,
    auth: authSlice,
    question: questionsSlice,
    result: resultSlice,
  },
});

export default store;
